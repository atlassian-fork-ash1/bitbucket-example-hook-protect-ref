package com.atlassian.bitbucket.repository.hook.ref;

import com.atlassian.bitbucket.hook.HookResponse;
import com.atlassian.bitbucket.hook.repository.PreReceiveRepositoryHook;
import com.atlassian.bitbucket.hook.repository.RepositoryHookContext;
import com.atlassian.bitbucket.repository.Ref;
import com.atlassian.bitbucket.repository.RefChange;
import com.atlassian.bitbucket.repository.RefService;

import javax.annotation.Nonnull;
import java.util.Collection;

public class ProtectRefHook implements PreReceiveRepositoryHook {

    private final RefService refService;

    public ProtectRefHook(RefService refService) {
        this.refService = refService;
    }


    /**
     * Disables changes to a ref
     */
    @Override
    public boolean onReceive(@Nonnull RepositoryHookContext context,
                             @Nonnull Collection<RefChange> refChanges,
                             @Nonnull HookResponse hookResponse) {
        String refId = context.getSettings().getString("ref-id");
        Ref found = refService.resolveRef(context.getRepository(), refId);
        for (RefChange refChange : refChanges) {
            if (refChange.getRefId().equals(found.getId())) {
                hookResponse.err().println("The ref '" + refChange.getRefId() + "' cannot be altered.");
                return false;
            }
        }
        return true;
    }
}
